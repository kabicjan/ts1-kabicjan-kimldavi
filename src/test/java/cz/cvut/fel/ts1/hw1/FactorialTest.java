package cz.cvut.fel.ts1.hw1;

import cz.cvut.fel.ts1.hw1.Factorial;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {

    private Factorial factorial=new Factorial();

    @Test
    void testFactorialWorkingCorrectly() {
        int result=120;
        assertEquals(factorial.factorial(5),result);
    }
    @Test
    void testFactorial2WorkingCorrectly() {
        int result=120;
        assertEquals(factorial.factorial2(5),result);
    }
    @Test
    void testBothWorkingCorrectlyForDiffNUmber() {
        for (int i = 0; i <10 ; i++) {
            assertEquals(factorial.factorial(i),factorial.factorial2(i));
        }
    }





}