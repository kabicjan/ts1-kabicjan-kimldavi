package cz.cvut.fel.ts1.hw1;

public class Factorial {
    public int factorial(int n) {
        if (n == 1) return 1;
        return n * (factorial(n - 1));
    }

    public int factorial2(int n) {
        int num = 1;
        for (int i = n; i > 0; i--) {
            num *= i;
        }
        return num;
    }
}
